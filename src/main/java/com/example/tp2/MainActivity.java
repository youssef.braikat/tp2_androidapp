package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {


    ListView listView;
    Cursor cursor;
    SimpleCursorAdapter adapter;
    WineDbHelper WineDbHelper;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar); // toolbar
        setSupportActionBar(toolbar);

        WineDbHelper = new WineDbHelper(getApplicationContext());
        // pour remplir la bd
        WineDbHelper.populate();


        cursor = WineDbHelper.fetchAllWines();
        cursor.moveToFirst();





        adapter = new SimpleCursorAdapter(

                this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[]{

                        WineDbHelper.COLUMN_NAME,
                        WineDbHelper.COLUMN_WINE_REGION
                },                              //Tableau de colonnes  auxquelles on va  lier de curseur
                new int[]{android.R.id.text1, android.R.id.text2

                },                             // les views lie a ces colonnes
                0);

        //supprimer a Wine
        listView = (ListView) findViewById(R.id.listView);
       registerForContextMenu(listView);
        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

                menu.add(Menu.NONE, 1, Menu.NONE, "delet");

            }
        });



      // Cliquer sur un item d'une listview pour lancer wineactivité

        listView.setAdapter(adapter);

       listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                Intent myIntent = new Intent(v.getContext(), WineActivity.class);
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                Wine Selected = WineDbHelper.cursorToWine(cursor);
                Log.d("Ovrire activity wine", Selected.toString());
                myIntent.putExtra("selection", Selected);
                startActivity(myIntent);
            }
        });



    //l'action de button ajouter (+)
      FloatingActionButton fab = findViewById(R.id.fab);
       fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), WineActivity.class);
                myIntent.putExtra("selection", (Bundle) null);
                startActivity(myIntent);

            }
        });
    }

    // prndre en charge les selection d'entre du menu contextuel ,on utilisant AdapterContextMenuInfo  spécifique a  AdapterViewc et ca inclut une ref a la view qui declanch le menu  et a partir de l'index donne la supprision se fait
    // pour mettre ajour notre liste
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        cursor.moveToPosition(info.position);
        WineDbHelper.deleteWine(cursor);
        cursor = WineDbHelper.fetchAllWines();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();

        return true;
    }


//pour mettre a jour l'activite et reconstruir les donnees a chaque interventions par la methode onResume()
    @Override
    protected void onResume() {
        super.onResume();
        cursor = WineDbHelper.fetchAllWines();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();

    }


}
